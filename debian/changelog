mono (6.12.0.114-0labft22) stable; urgency=medium

  * MeasureTextInternal принимает Font = null.
  * Make ToolStripMenuItem constructor  handle shortcut parameter.
  * Fixed dependence of the countdown in the
    System.Windows.Forms.Timer on the system time.
  * DataGridView: Update editing control position on
    scroll.
  * Form: Set `WM_CLASS` while change `Text`.
  * GridEnty: Invoke type converter to display entry
    value properly.
  * ContextMenuStrip: Close toplevel menu if some
    click somewhere outside of it.
  * Fix changing reference to PrintController in
    preview in PrintPreviewControl.
  * Improve compatibility of property grid custom editors.

 -- Лаборатория 50 <team@lab50.net>  Wed, 03 Apr 2024 12:45:40 +0300

mono (6.12.0.114-0labft21) testing; urgency=medium

  * Добавлен пакет libmono-cecil-cil.
  * Добавлен SDK Microsoft.Build.NoTargets.

 -- Лаборатория 50 <team@lab50.net>  Tue, 03 Oct 2023 10:06:21 +0300

mono (6.12.0.114-0labft20) testing; urgency=medium

  * Добавлена зависимость для libmono-2.0-dev.
  * Добавлен патч remoting-version-check.patch.

 -- Лаборатория 50 <team@lab50.net>  Fri, 29 Sep 2023 14:34:38 +0300

mono (6.12.0.114-0labft18) testing; urgency=medium

  * monodoc-manual переведен в рекомендуемые.

 -- Лаборатория 50 <team@lab50.net>  Thu, 21 Oct 2021 14:40:00 +0300

mono (6.12.0.114-0labft17) testing; urgency=medium

  * Пакет referenceassemblies-pcl заменен на mono-reference-assemblies-
    cil.
  * monodoc-manual не зависит от архитетуры.

 -- Лаборатория 50 <team@lab50.net>  Thu, 21 Oct 2021 12:28:46 +0300

mono (6.12.0.114-0labft16) 2021.1; urgency=medium

  * Добавлены сведения о версии.

 -- Лаборатория 50 <team@lab50.net>  Fri, 11 Jun 2021 13:07:04 +0300

mono (6.12.0.114-0labft15) testing; urgency=medium

  * Очистка /etc/mono/registry после полного удаления пакета mono-runtime.

 -- Лаборатория 50 <team@lab50.net>  Wed, 09 Jun 2021 13:27:28 +0300

mono (6.12.0.114-0labft14) testing; urgency=medium

  * Очистка /etc/mono/certstore после полного удаления пакетов.
  * Вызов cert-sync только при его наличии.

 -- Лаборатория 50 <team@lab50.net>  Wed, 09 Jun 2021 12:05:06 +0300

mono (6.12.0.114-0labft13) testing; urgency=medium

  * Возможность подписи ЗПС ALSE.

 -- Лаборатория 50 <team@lab50.net>  Fri, 23 Apr 2021 14:27:35 +0300

mono (6.12.0.114-0labft12) testing; urgency=medium

  * Генерация AOT образов для сертифицированной версии.
  * Возвращёна сборка Microsoft.CSharp.
  * Улучшена совместимость с OpenSSL.
  * Удалёна сборка Mono.Data.Tds.

 -- Лаборатория 50 <team@lab50.net>  Fri, 02 Apr 2021 12:16:30 +0300

mono (6.12.0.114-0labft11) testing; urgency=medium

  * Удалены одинаковые в файлы в libmono и libmono-dev.

 -- Лаборатория 50 <team@lab50.net>  Fri, 05 Mar 2021 17:37:13 +0300

mono (6.12.0.114-0labft10) testing; urgency=medium

  * Класс System.Runtime.CompilerServices.Unsafe теперь публичный в
    mscorlib.
  * Удалена публичная информация из кода BTLS.

 -- Лаборатория 50 <team@lab50.net>  Thu, 04 Mar 2021 12:21:34 +0300

mono (6.12.0.114-0labft9) testing; urgency=medium

  * Исправлено падение при использовании SSL/TSL.

 -- Лаборатория 50 <team@lab50.net>  Mon, 01 Mar 2021 13:04:15 +0300

mono (6.12.0.114-0labft8) testing; urgency=medium

  * Удалены упоминания mono-runtime-common.

 -- Лаборатория 50 <team@lab50.net>  Wed, 24 Feb 2021 12:46:41 +0300

mono (6.12.0.114-0labft7) testing; urgency=medium

  * Объединены пакеты mono-runtime-common и mono-runtime.
  * Уровень debhelper повышен до 10.
  * Возвращен пакет mono-jay.
  * Обработка профилей nodoc, cert.
  * Использование системного заголовочного файла zlib.h.
  * DH_INTERNAL_MONO_PARAM больше не используется.

 -- Лаборатория 50 <team@lab50.net>  Sat, 13 Feb 2021 10:57:40 +0300

mono (6.12.0.114-0labft6) testing; urgency=medium

  * Пакеты libmonosgen* объединены с libmono*.
  * Пакет mono-runtime-sgen объединен с mono-runtime.

 -- Лаборатория 50 <team@lab50.net>  Thu, 21 Jan 2021 20:58:51 +0300

mono (6.12.0.114-0labft5) testing; urgency=medium

  * Исправлены права на файл в runtime.d.

 -- Лаборатория 50 <team@lab50.net>  Thu, 21 Jan 2021 17:48:25 +0300

mono (6.12.0.114-0labft4) testing; urgency=medium

  * Использование ERR_print_errors вместо BIO_print_errors.

 -- Лаборатория 50 <team@lab50.net>  Thu, 21 Jan 2021 16:10:27 +0300

mono (6.12.0.114-0labft3) testing; urgency=medium

  * Установка файлов перенесена из rules в *.install.
  * Сборка с mono-microsoft-net-compilers-toolset или mono-roslyn.
  * Сборка с mono-reference-assemblies-cil или mono-devel.
  * Удалены файлы /usr/lib/mono/4.5/Facades из mono-devel.

 -- Лаборатория 50 <team@lab50.net>  Mon, 18 Jan 2021 19:07:36 +0300

mono (6.12.0.114-0labft1) testing; urgency=medium

  * New upstream version.

 -- Лаборатория 50 <team@lab50.net>  Thu, 14 Jan 2021 15:48:17 +0300

mono (6.12.0.112-0labft2) testing; urgency=medium

  * Initial release.
  * Dist clean pdb and exe files.
  * Добавлены недостающие файлы тестовых утилит.
  * Use system OpenSSL instead of BoringTLS.
  * Dropped deprecated MCS compiler.
  * Dropped deprecated XBuild.
  * Сборка с помощью внешней утилиты jay.

 -- Лаборатория 50 <team@lab50.net>  Fri, 18 Dec 2020 08:54:59 +0300
